from django.contrib import admin
from django.urls import path, include
from receipts.views import create_receipt, receipt_list


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
]
